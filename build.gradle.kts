plugins {
    java
    jacoco
}

repositories {
    jcenter()
}

dependencies {
    testCompileOnly("junit", "junit", "4.+")

    testImplementation("org.junit.jupiter", "junit-jupiter-api", "5.+")

    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", "5.+")
    testRuntimeOnly("org.junit.vintage", "junit-vintage-engine", "5.+")
}
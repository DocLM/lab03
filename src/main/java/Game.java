import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Game {
    private final static int MAX_FRAMES = 10;

    private List<Frame> frames;
    private int currentFrame = 0;

    public Game() {
        this.frames = Stream
                .generate(Frame::new)
                .limit(MAX_FRAMES)
                .collect(Collectors.toList());
    }

    public void roll(int pins) throws Exception
    {
            Frame frame = frames.get(currentFrame);
            frame.roll(pins);
            if (frame.isCompleted())
            {
                this.updateGame();
                this.currentFrame++;
            }
    }

    private void updateGame()
    {
        if (currentFrame >= MAX_FRAMES - 2 && currentFrame < MAX_FRAMES)
        {
            Frame frame = frames.get(currentFrame);
            if (frame.moves() == 1 && frame.score() == 10)
            {
                this.frames.addAll(Stream
                        .generate(Frame::new)
                        .limit(currentFrame == MAX_FRAMES - 1 ? 2 : 1)
                        .collect(Collectors.toList())
                );
                return;
            }
            if (frame.moves() == 2 && currentFrame == MAX_FRAMES - 1 && frame.score() == 10)
            {
                this.frames.add(new Frame(10, 1));
                return;
            }
        }
    }

    public int score() {
        int totalScore = 0;
        for (int i = 0; i < MAX_FRAMES - 2; i++)
        {
            Frame frame = this.frames.get(i);
            totalScore += frame.score();
            if (frame.score() == 10)
                for (int j = 1; j <= 2 - frame.moves() + 1; j++)
                    totalScore += frames.get(i + j).score();
        }

        for (int i = MAX_FRAMES - 2; i < frames.size(); i++)
            totalScore += frames.get(i).score();

        return totalScore;
    }
}

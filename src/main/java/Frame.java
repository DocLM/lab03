public class Frame {
    private final int MAX_MOVES;
    private final int MAX_PINS;

    private int moves = 0;
    private int pins;

    public Frame() {
        this.MAX_MOVES = 2;
        this.MAX_PINS  = 10;
    }
    public Frame(int pins, int moves)
    {
        this.MAX_PINS  = pins;
        this.MAX_MOVES = moves;
    }

    public void roll(int pins)
    {
        if (this.rollable(pins))
        {
            this.pins += pins;
            this.moves++;
            return;
        }

        throw new IllegalArgumentException("Illegal moves");
    }

    protected boolean rollable(int pins) {
        return this.pins + pins <= MAX_PINS && this.moves < MAX_MOVES;
    }

    public boolean isCompleted() {
        return this.pins == MAX_PINS || this.moves == MAX_MOVES;
    }

    public int score() {
        return this.pins;
    }

    public int moves() {
        return this.moves;
    }
}
